package com.clear.usermanagement.response;

import java.util.List;
import lombok.Data;

@Data
public class ResponseList {
    
    private String status;
    private List<?> data;
    private String message;
    
}
