package com.clear.usermanagement.request.account;

import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class AccountRequest {
    
    private Integer account_id;
    
    @NotBlank
    private String username;
    
    @NotBlank
    private String password;
            
    @NotBlank
    private String email;
    
    private Integer activated;
    
    private Integer banned;
    
    private String banned_reason;
    
    private String fullname;
     
    private Integer jobtitle_id;
    
    private List<Integer> role;
    
    @NotNull
    private Integer company_id;
    
    private Integer updated_by;
    
    private Date updated_at;
    
}
