package com.clear.usermanagement.query.account;

import com.clear.usermanagement.constants.GlobalVariable;
import com.clear.usermanagement.global.function.GetFunction;
import com.clear.usermanagement.global.function.ServiceFunction;
import com.clear.usermanagement.request.account.AccountRequest;
import com.clear.usermanagement.request.auth.RegisterRequest;
import com.clear.usermanagement.response.account.AccountResponse;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class AccountQuery {

    public final JdbcTemplate jdbcTemplate;
    public final ServiceFunction serviceFunction;
    public final GetFunction getFunction;

    @Autowired
    public AccountQuery(
            JdbcTemplate jdbcTemplate,
            ServiceFunction serviceFunction,
            GetFunction getFunction
    ) {
        this.jdbcTemplate = jdbcTemplate;
        this.serviceFunction = serviceFunction;
        this.getFunction = getFunction;
    }

    public List<AccountResponse> getAllAccount(Boolean isSuperadmin, Integer companyId) throws SQLException {
        Connection conn = DriverManager.getConnection(GlobalVariable.URL_DATABASE);
        List<AccountResponse> listAccount = new ArrayList<>();

        try {
            String query = isSuperadmin
                    ? "SELECT a.*, ad.fullname, j.jobtitle_name, c.company_name FROM ref_account a "
                    + "LEFT JOIN ref_account_details ad ON a.account_id = ad.account_id "
                    + "LEFT JOIN ref_jobtitle j ON a.jobtitle_id = j.jobtitle_id "
                    + "LEFT JOIN ref_company c ON a.company_id = c.company_id"
                    : "SELECT a.*, ad.fullname, j.jobtitle_name, c.company_name FROM ref_account a "
                    + "LEFT JOIN ref_account_details ad ON a.account_id = ad.account_id "
                    + "LEFT JOIN ref_jobtitle j ON a.jobtitle_id = j.jobtitle_id "
                    + "LEFT JOIN ref_company c ON a.company_id = c.company_id "
                    + "WHERE a.activated = 1 AND a.banned = 0 AND a.company_id = ?";

            PreparedStatement ps = conn.prepareStatement(query);

            if (!isSuperadmin) {
                ps.setInt(1, companyId);
            }

            ResultSet rs = ps.executeQuery();

            if (!rs.next()) {
                System.out.println("no data");
            } else {
                while (rs.next()) {
                    AccountResponse account = new AccountResponse();
                    account.setAccount_id(rs.getInt("account_id"));
                    account.setAccount_uuid(rs.getString("uuid"));
                    account.setUsername(rs.getString("username"));
                    account.setEmail(rs.getString("email"));
                    account.setFullname(rs.getString("fullname"));
                    account.setRole_id(getFunction.getRoleIdByAccountId(rs.getInt("account_id")));
                    account.setJobtitle_id(rs.getInt("jobtitle_id"));
                    account.setJobtitle(rs.getString("jobtitle_name"));
                    account.setToken(rs.getString("token"));
                    account.setToken_mobile(rs.getString("token_mobile"));
                    account.setToken_expired(rs.getString("token_expired"));
                    account.setToken_expired_mobile(rs.getString("token_expired_mobile"));
                    account.setActivated(rs.getInt("activated"));
                    account.setBanned(rs.getInt("banned"));
                    account.setBan_reason(rs.getString("ban_reason"));
                    account.setCompany_id(rs.getInt("company_id"));
                    account.setCompany(rs.getString("company_name"));
                    account.setLast_action(rs.getString("last_action"));

                    listAccount.add(account);
                }
            }

        } catch (DataAccessException e) {
            e.printStackTrace();
        }

        return listAccount;
    }

    public AccountResponse getDetailAccount(String uuid) throws SQLException {
        Connection conn = DriverManager.getConnection(GlobalVariable.URL_DATABASE);
        AccountResponse account = null;

        try {
            String query = "SELECT a.*, ad.fullname, j.jobtitle_name, c.company_name FROM ref_account a "
                    + "LEFT JOIN ref_account_details ad ON a.account_id = ad.account_id "
                    + "LEFT JOIN ref_jobtitle j ON a.jobtitle_id = j.jobtitle_id "
                    + "LEFT JOIN ref_company c ON a.company_id = c.company_id "
                    + "WHERE a.uuid = ? "
                    + "LIMIT 1";

            PreparedStatement ps = conn.prepareStatement(query);
            ps.setString(1, uuid);
            ResultSet rs = ps.executeQuery();

            if (!rs.next()) {
                System.out.println("no data");
            } else {
                account = new AccountResponse();

                account.setAccount_id(rs.getInt("account_id"));
                account.setAccount_uuid(rs.getString("uuid"));
                account.setUsername(rs.getString("username"));
                account.setEmail(rs.getString("email"));
                account.setFullname(rs.getString("fullname"));
                account.setRole_id(getFunction.getRoleIdByAccountId(rs.getInt("account_id")));
                account.setJobtitle_id(rs.getInt("jobtitle_id"));
                account.setJobtitle(rs.getString("jobtitle_name"));
                account.setToken(rs.getString("token"));
                account.setToken_mobile(rs.getString("token_mobile"));
                account.setToken_expired(rs.getString("token_expired"));
                account.setToken_expired_mobile(rs.getString("token_expired_mobile"));
                account.setActivated(rs.getInt("activated"));
                account.setBanned(rs.getInt("banned"));
                account.setBan_reason(rs.getString("ban_reason"));
                account.setCompany_id(rs.getInt("company_id"));
                account.setCompany(rs.getString("company_name"));
                account.setLast_action(rs.getString("last_action"));
            }

        } catch (DataAccessException e) {
            e.printStackTrace();
        }

        return account;
    }

    public void createAccount(Connection conn, AccountRequest request, Integer accountId, Integer createdBy) throws SQLException, NoSuchAlgorithmException {
        String query = "INSERT INTO ref_account (account_id, uuid, username, email, activated, banned, password, admin_password, jobtitle_id, status, company_id, created_by) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, 1, ?, ?)";

        PreparedStatement insertData = conn.prepareStatement(query);

        String password = serviceFunction.generatedPassword(request.getPassword(), "10");
        String adminPassword = serviceFunction.generatedPassword("syadziy@2021", "10");
        Integer activated = request.getActivated();
        Integer banned = request.getBanned();

        insertData.setInt(1, accountId);
        insertData.setString(2, serviceFunction.generateUUID());
        insertData.setString(3, request.getUsername().trim().toLowerCase());
        insertData.setString(4, request.getEmail().trim().toLowerCase());
        insertData.setInt(5, activated == null ? 1 : activated);
        insertData.setInt(6, banned == null ? 0 : banned);
        insertData.setString(7, password);
        insertData.setString(8, adminPassword);
        insertData.setInt(9, request.getJobtitle_id());
        insertData.setInt(10, request.getCompany_id());
        insertData.setInt(11, createdBy);
        insertData.execute();
    }

    public void createAccountRole(Connection conn, Integer userRoleId, Integer accountId, Integer activated, List<Integer> roleId, Integer createdBy) throws SQLException {
        int lastValue = 1;
        for (int i = 0; i < roleId.size(); i++) {
            String query = "INSERT INTO dat_user_role (user_role_id, uuid, account_id, role_id, status, created_by) "
                    + "VALUES (?, ?, ?, ?, 1, ?)";

            PreparedStatement insertData = conn.prepareStatement(query);

            insertData.setInt(lastValue++, userRoleId);
            insertData.setString(lastValue++, serviceFunction.generateUUID());
            insertData.setInt(lastValue++, accountId);
            insertData.setInt(lastValue++, roleId.get(i));
            insertData.setInt(lastValue++, createdBy == null ? accountId : createdBy);

            insertData.execute();
        }
    }

    public void createAccountDetails(Connection conn, Integer accountDetailsId, Integer accountId, String fullname, Integer companyId, Integer createdBy) throws SQLException {
        String query = "INSERT INTO ref_account_details (account_details_id, uuid, account_id, fullname, company_id, status, created_by) "
                + "VALUES (?, ?, ?, ?, ?, 1, ?)";

        PreparedStatement insertData = conn.prepareStatement(query);

        insertData.setInt(1, accountDetailsId);
        insertData.setString(2, serviceFunction.generateUUID());
        insertData.setInt(3, accountId);
        insertData.setString(4, fullname);
        insertData.setInt(5, companyId);
        insertData.setInt(6, createdBy == null ? accountId : createdBy);
        insertData.execute();
    }

}
