package com.clear.usermanagement.query.activity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.springframework.stereotype.Service;

@Service
public class ActivityQuery {
    
    public void createActivity(Connection conn, Integer activityId, Integer accountId, String activity, String activityType, Integer moduleId, String modules, String url, String uuid) throws SQLException {
        String query = moduleId != null
                ? "INSERT INTO dat_activity (activity_id, uuid, account_id, activity, activity_type, module_id, modules, url, created_by) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)"
                : "INSERT INTO dat_activity (activity_id, uuid, account_id, activity, activity_type, module_id, modules, url, created_by) "
                + "VALUES (?, ?, ?, ?, ?, NULL, ?, ?, ?)";

        PreparedStatement insertData = conn.prepareStatement(query);

        insertData.setInt(1, activityId);
        insertData.setString(2, uuid);
        insertData.setInt(3, accountId);
        insertData.setString(4, activity);
        insertData.setString(5, activityType);

        if (moduleId != null) {
            insertData.setInt(6, moduleId);
            insertData.setString(7, modules);
            insertData.setString(8, url);
            insertData.setInt(9, accountId);
        } else {
            insertData.setString(6, modules);
            insertData.setString(7, url);
            insertData.setInt(8, accountId);
        }

        insertData.execute();
    }
    
}
