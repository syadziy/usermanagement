package com.clear.usermanagement.dao;

import com.clear.usermanagement.request.account.AccountRequest;
import com.clear.usermanagement.response.Response;
import org.springframework.http.ResponseEntity;

public interface AccountDao {
    
    ResponseEntity getAllData(Response response, String url);
    ResponseEntity getDetailData(Response response, String url, String uuid);
    ResponseEntity createData(Response response, AccountRequest request, String url);
    
}
