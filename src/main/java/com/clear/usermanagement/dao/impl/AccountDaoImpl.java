package com.clear.usermanagement.dao.impl;

import com.clear.usermanagement.constants.GlobalVariable;
import com.clear.usermanagement.dao.AccountDao;
import com.clear.usermanagement.entity.master.AccountEntity;
import com.clear.usermanagement.global.function.GetFunction;
import com.clear.usermanagement.global.function.GlobalFunction;
import com.clear.usermanagement.global.function.ServiceFunction;
import com.clear.usermanagement.language.English;
import com.clear.usermanagement.query.account.AccountQuery;
import com.clear.usermanagement.request.account.AccountRequest;
import com.clear.usermanagement.response.Response;
import com.clear.usermanagement.response.account.AccountResponse;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

@Repository
public class AccountDaoImpl implements AccountDao {

    public final ServiceFunction serviceFunction;
    public final GlobalFunction globalFunction;
    public final GetFunction getFunction;
    public final AccountQuery accountQuery;
    public final HttpServletRequest httpServletRequest;

    @Autowired
    public AccountDaoImpl(
            ServiceFunction serviceFunction,
            GlobalFunction globalFunction,
            GetFunction getFunction,
            AccountQuery accountQuery,
            HttpServletRequest httpServletRequest
    ) {
        this.serviceFunction = serviceFunction;
        this.globalFunction = globalFunction;
        this.getFunction = getFunction;
        this.accountQuery = accountQuery;
        this.httpServletRequest = httpServletRequest;
    }

    @Override
    public ResponseEntity getAllData(Response response, String url) {
        try {
            ResponseEntity<?> token = globalFunction.checkToken(httpServletRequest, response, null, url);
            int statusCode = token.getStatusCodeValue();

            if (statusCode != 100) {
                return token;
            }

            AccountEntity dataToken = getFunction.getDataToken(httpServletRequest);
            List<AccountResponse> listAccount = accountQuery.getAllAccount(globalFunction.isSuperadmin(dataToken.getAccountId()), dataToken.getCompanyId());

            if (globalFunction.checkActivityConfig(dataToken.getAccountId(), dataToken.getCompanyId())) {
                serviceFunction.generateActivity(
                        serviceFunction.generateID("dat_activity"),
                        dataToken.getAccountId(),
                        ("Get all data account"),
                        "get-data",
                        null,
                        "account",
                        "api/account/get_all_account"
                );
            }

            serviceFunction.generateLastAction(dataToken.getAccountId(), dataToken.getCompanyId());

            response.list.setData(listAccount);
            response.list.setStatus(English.success);
            response.list.setMessage(English.getDataSuccess);
            return new ResponseEntity(response.list, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            response.object.setMessage(English.internalServerError);
            return new ResponseEntity(response.object, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity getDetailData(Response response, String url, String uuid) {
        try {
            ResponseEntity<?> token = globalFunction.checkToken(httpServletRequest, response, null, url);
            int statusCode = token.getStatusCodeValue();

            if (statusCode != 100) {
                return token;
            }

            AccountEntity dataToken = getFunction.getDataToken(httpServletRequest);
            AccountResponse dataAccount = accountQuery.getDetailAccount(uuid);

            if (dataAccount == null) {
                response.object.setMessage(English.userNotFound);
                return new ResponseEntity(response.object, HttpStatus.OK);
            }

            Optional<?> account = new ArrayList(Arrays.asList(dataAccount)).stream().findFirst();

            if (globalFunction.checkActivityConfig(dataToken.getAccountId(), dataToken.getCompanyId())) {
                serviceFunction.generateActivity(
                        serviceFunction.generateID("dat_activity"),
                        dataToken.getAccountId(),
                        ("Get detail data account"),
                        "get-data",
                        null,
                        "account",
                        "api/account/get_detail_account"
                );
            }

            serviceFunction.generateLastAction(dataToken.getAccountId(), dataToken.getCompanyId());

            response.object.setData(account);
            response.object.setStatus(English.success);
            response.object.setMessage(English.getDataSuccess);
            return new ResponseEntity(response.object, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            response.object.setMessage(English.internalServerError);
            return new ResponseEntity(response.object, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity createData(Response response, AccountRequest request, String url) {
        try {
            Connection conn = DriverManager.getConnection(GlobalVariable.URL_DATABASE);
            conn.setAutoCommit(false);

            try {
                ResponseEntity<?> token = globalFunction.checkToken(httpServletRequest, response, request.getCompany_id(), url);
                int statusCode = token.getStatusCodeValue();

                if (statusCode != 100) {
                    return token;
                }

                AccountEntity dataToken = getFunction.getDataToken(httpServletRequest);

                Integer accountId = serviceFunction.generateID("ref_account");
                Integer accountDetailsId = serviceFunction.generateID("ref_account_details");
                Integer userRoleId = serviceFunction.generateID("dat_user_role");
                Integer activated = request.getActivated();
                String username = request.getUsername().trim().toLowerCase();
                String email = request.getEmail().trim().toLowerCase();

                // Cek akun exist
                if (globalFunction.checkAccountExist(username, email, request.getCompany_id())) {
                    response.object.setMessage(English.userExist);
                    return new ResponseEntity(response.object, HttpStatus.OK);
                }

                accountQuery.createAccount(conn, request, accountId, dataToken.getAccountId());

                if (request.getRole() != null) {
                    List<Integer> roleId = request.getRole();
                    accountQuery.createAccountDetails(conn, accountDetailsId, accountId, request.getFullname(), request.getCompany_id(), dataToken.getAccountId());

                    if (!roleId.isEmpty()) {
                        accountQuery.createAccountRole(conn, userRoleId, accountId, activated, roleId, dataToken.getAccountId());
                    }
                }

                if (globalFunction.checkActivityConfig(dataToken.getAccountId(), dataToken.getCompanyId())) {
                    serviceFunction.generateActivity(
                            serviceFunction.generateID("dat_activity"),
                            dataToken.getAccountId(),
                            ("Create data account"),
                            "create",
                            null,
                            "account",
                            "api/account/create_account"
                    );
                }

                serviceFunction.generateLastAction(dataToken.getAccountId(), dataToken.getCompanyId());

                conn.commit();
                response.object.setStatus(English.success);
                response.object.setMessage(English.createDataSuccess);
                return new ResponseEntity(response.object, HttpStatus.OK);

            } catch (SQLException sqlexcep) {
                conn.rollback();
                sqlexcep.printStackTrace();
                response.object.setMessage(English.internalServerError);
                return new ResponseEntity(response.object, HttpStatus.INTERNAL_SERVER_ERROR);
            } catch (ParseException peexcep) {
                conn.rollback();
                peexcep.printStackTrace();
                response.object.setMessage(English.internalServerError);
                return new ResponseEntity(response.object, HttpStatus.INTERNAL_SERVER_ERROR);
            } catch (NoSuchAlgorithmException nosuchexcep) {
                conn.rollback();
                nosuchexcep.printStackTrace();
                response.object.setMessage(English.internalServerError);
                return new ResponseEntity(response.object, HttpStatus.INTERNAL_SERVER_ERROR);
            } finally {
                conn.close();
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
            response.object.setMessage(English.internalServerError);
            return new ResponseEntity(response.object, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
