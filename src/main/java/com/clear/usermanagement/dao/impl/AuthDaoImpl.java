package com.clear.usermanagement.dao.impl;

import com.clear.usermanagement.constants.GlobalVariable;
import com.clear.usermanagement.dao.AuthDao;
import com.clear.usermanagement.entity.master.AccountEntity;
import com.clear.usermanagement.entity.master.TokenConfigEntity;
import com.clear.usermanagement.global.function.GetFunction;
import com.clear.usermanagement.global.function.GlobalFunction;
import com.clear.usermanagement.global.function.ServiceFunction;
import com.clear.usermanagement.language.English;
import com.clear.usermanagement.global.query.CreateQuery;
import com.clear.usermanagement.global.query.UpdateQuery;
import com.clear.usermanagement.query.account.AccountQuery;
import com.clear.usermanagement.repository.master.AccountRepository;
import com.clear.usermanagement.repository.master.TokenConfigRepository;
import com.clear.usermanagement.request.auth.ActivatedAccountRequest;
import org.springframework.stereotype.Repository;
import com.clear.usermanagement.request.auth.LoginRequest;
import com.clear.usermanagement.request.auth.RegisterRequest;
import com.clear.usermanagement.response.Response;
import com.clear.usermanagement.response.auth.LoginResponse;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@Repository
public class AuthDaoImpl implements AuthDao {

    public final ServiceFunction serviceFunction;
    public final GlobalFunction globalFunction;
    public final GetFunction getFunction;
    public final CreateQuery createQuery;
    public final UpdateQuery updateQuery;
    public final AccountQuery accountQuery;
    public final HttpServletRequest httpServletRequest;
    public final AccountRepository accountRepository;
    public final TokenConfigRepository tokenConfigRepository;

    @Autowired
    public AuthDaoImpl(
            ServiceFunction serviceFunction,
            GlobalFunction globalFunction,
            GetFunction getFunction,
            CreateQuery createQuery,
            UpdateQuery updateQuery,
            AccountQuery accountQuery,
            HttpServletRequest httpServletRequest,
            AccountRepository accountRepository,
            TokenConfigRepository tokenConfigRepository
    ) {
        this.serviceFunction = serviceFunction;
        this.globalFunction = globalFunction;
        this.getFunction = getFunction;
        this.createQuery = createQuery;
        this.updateQuery = updateQuery;
        this.accountQuery = accountQuery;
        this.httpServletRequest = httpServletRequest;
        this.accountRepository = accountRepository;
        this.tokenConfigRepository = tokenConfigRepository;
    }

    @Override
    public ResponseEntity login(Response response, LoginRequest request) {
        try {
            String username = request.getUsername().trim().toLowerCase();
            String ip = httpServletRequest.getRemoteAddr();
            String passwordEncripted = serviceFunction.generatedPassword(request.getPassword(), "10");
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            String tokenExpired;

            // Cek akun exist
            if (!globalFunction.checkUsername(username, request.getCompany_id())) {
                response.object.setMessage(English.userNotFound);
                return new ResponseEntity(response.object, HttpStatus.OK);
            }

            // Data akun
            AccountEntity account = request.getCompany_id() == null
                    ? accountRepository.findByUsernameAdmin(username)
                    : accountRepository.findByUsername(username, request.getCompany_id());

            // Cek allowed ip
            if (!globalFunction.checkAllowedIp(ip, account.getAccountId(), account.getCompanyId())) {
                response.object.setMessage(English.ipNotAllowed);
                return new ResponseEntity(response.object, HttpStatus.OK);
            }

            // Cek username password
            if (!globalFunction.checkUsernamePassword(username, passwordEncripted, request.getCompany_id())) {
                response.object.setMessage(English.incorrectPassword);
                return new ResponseEntity(response.object, HttpStatus.OK);
            }

            // Cek akun
            if (account.getActivated() != 1) {
                response.object.setMessage(English.userNotActivated);
                return new ResponseEntity(response.object, HttpStatus.OK);
            } else if (account.getBanned() != 0) {
                response.object.setMessage(English.userBanned);
                return new ResponseEntity(response.object, HttpStatus.OK);
            }

            if (globalFunction.isSuperadmin(account.getAccountId())) {
                cal.add(Calendar.YEAR, 1);

                tokenExpired = formatter.format(cal.getTime());
                String tokenEncripted = serviceFunction.generatedToken(request.getPassword(), "100", null);

                account.setToken(tokenEncripted);
            } else {
                TokenConfigEntity tokenConfig = tokenConfigRepository.findByCompanyId(account.getCompanyId());
                if (tokenConfig == null) {
                    response.object.setMessage(English.tokenConfigError);
                    return new ResponseEntity(response.object, HttpStatus.OK);
                }

                if (null != tokenConfig.getTypeExpired()) {
                    switch (tokenConfig.getTypeExpired()) {
                        case "year":
                            cal.add(Calendar.YEAR, tokenConfig.getExpired());
                            break;
                        case "month":
                            cal.add(Calendar.MONTH, tokenConfig.getExpired());
                            break;
                        case "day":
                            cal.add(Calendar.DATE, tokenConfig.getExpired());
                            break;
                        case "hour":
                            cal.add(Calendar.HOUR, tokenConfig.getExpired());
                            break;
                        case "minute":
                            cal.add(Calendar.MINUTE, tokenConfig.getExpired());
                            break;
                        default:
                            break;
                    }
                }

                tokenExpired = formatter.format(cal.getTime());
                String tokenEncripted = serviceFunction.generatedToken(request.getPassword(), "10", account.getCompanyId());

                if (!tokenConfig.getIsMultiLogin()) {
                    account.setToken(tokenEncripted);
                } else if (account.getToken() == null) {
                    account.setToken(tokenEncripted);
                }

            }

            account.setTokenExpired(tokenExpired);
            account.setLastAction(tokenExpired);

            accountRepository.save(account);

            List<Integer> accountRole = getFunction.getRoleIdByAccountId(account.getAccountId());

            LoginResponse stringList = new LoginResponse(
                    account.getAccountId(),
                    account.getUsername(),
                    account.getEmail(),
                    accountRole,
                    account.getToken(),
                    account.getTokenMobile(),
                    account.getTokenExpired(),
                    account.getTokenExpiredMobile(),
                    account.getCompanyId(),
                    account.getLastAction()
            );

            List<?> listData = new ArrayList(Arrays.asList(stringList));
            Optional<?> data = listData.stream().findFirst();

            if (globalFunction.checkActivityConfig(account.getAccountId(), account.getCompanyId())) {
                Calendar calendar = Calendar.getInstance();
                SimpleDateFormat formatterActivity = new SimpleDateFormat("EEEE, MMM dd yyyy");
                String currentTime = formatterActivity.format(calendar.getTime());

                serviceFunction.generateActivity(
                        serviceFunction.generateID("dat_activity"),
                        account.getAccountId(),
                        ("Login at " + currentTime),
                        "login",
                        null,
                        "auth",
                        "api/login"
                );
            }

            serviceFunction.generateLastAction(account.getAccountId(), account.getCompanyId());

            response.object.setData(data);
            response.object.setStatus(English.success);
            response.object.setMessage(English.loginSuccess);
            return new ResponseEntity(response.object, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            response.object.setMessage(English.internalServerError);
            return new ResponseEntity(response.object, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity register(Response response, RegisterRequest request) {
        try {
            Connection conn = DriverManager.getConnection(GlobalVariable.URL_DATABASE);
            conn.setAutoCommit(false);

            try {
                Integer accountId = serviceFunction.generateID("ref_account");
                Integer accountDetailsId = serviceFunction.generateID("ref_account_details");
                Integer userRoleId = serviceFunction.generateID("dat_user_role");
                Integer activated = 0;
                Integer banned = 0;
                String ip = httpServletRequest.getRemoteAddr();
                String username = request.getUsername().trim().toLowerCase();
                String email = request.getEmail().trim().toLowerCase();

                // Cek allowed ip
                if (!globalFunction.checkAllowedIp(ip, null, request.getCompany_id())) {
                    response.object.setMessage(English.ipNotAllowed);
                    return new ResponseEntity(response.object, HttpStatus.OK);
                }

                // Cek register akun company
                if (!globalFunction.checkIsRegisterAccount(request.getCompany_id())) {
                    response.object.setMessage(English.registerNotAllowed);
                    return new ResponseEntity(response.object, HttpStatus.OK);
                }

                // Cek akun exist
                if (globalFunction.checkAccountExist(username, email, request.getCompany_id())) {
                    response.object.setMessage(English.userExist);
                    return new ResponseEntity(response.object, HttpStatus.OK);
                }

                createQuery.registerAccount(conn, request, accountId, activated, banned);

                if (request.getRole() != null) {
                    List<Integer> roleId = request.getRole();
                    accountQuery.createAccountDetails(conn, accountDetailsId, accountId, request.getFullname(), request.getCompany_id(), null);

                    if (!roleId.isEmpty()) {
                        accountQuery.createAccountRole(conn, userRoleId, accountId, activated, roleId, null);
                    }
                }

                if (globalFunction.checkActivityConfig(accountId, request.getCompany_id())) {
                    serviceFunction.generateActivity(
                            serviceFunction.generateID("dat_activity"),
                            accountId,
                            ("Register for account " + username),
                            "register",
                            null,
                            "auth",
                            "api/register"
                    );
                }

                serviceFunction.generateLastAction(accountId, request.getCompany_id());

                conn.commit();
                response.object.setStatus(English.success);
                response.object.setMessage(English.registerSuccess);
                return new ResponseEntity(response.object, HttpStatus.OK);

            } catch (SQLException sqlexcep) {
                conn.rollback();
                sqlexcep.printStackTrace();
                response.object.setMessage(English.internalServerError);
                return new ResponseEntity(response.object, HttpStatus.INTERNAL_SERVER_ERROR);
            } catch (ParseException peexcep) {
                conn.rollback();
                peexcep.printStackTrace();
                response.object.setMessage(English.internalServerError);
                return new ResponseEntity(response.object, HttpStatus.INTERNAL_SERVER_ERROR);
            } catch (NoSuchAlgorithmException nosuchexcep) {
                conn.rollback();
                nosuchexcep.printStackTrace();
                response.object.setMessage(English.internalServerError);
                return new ResponseEntity(response.object, HttpStatus.INTERNAL_SERVER_ERROR);
            } finally {
                conn.close();
            }

        } catch (SQLException e) {
            e.printStackTrace();
            response.object.setMessage(English.internalServerError);
            return new ResponseEntity(response.object, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity activatedAccount(Response response, ActivatedAccountRequest request) {
        try {
            Connection conn = DriverManager.getConnection(GlobalVariable.URL_DATABASE);
            conn.setAutoCommit(false);

            try {
                String ip = httpServletRequest.getRemoteAddr();

                // Cek allowed ip
                if (!globalFunction.checkAllowedIp(ip, null, request.getCompany_id())) {
                    response.object.setMessage(English.ipNotAllowed);
                    return new ResponseEntity(response.object, HttpStatus.OK);
                }

                // Cek akun sudah aktifasi
                if (globalFunction.checkAccountExistByAccountId(request.getAccount_id(), request.getCompany_id())) {
                    response.object.setMessage(English.userAlreadyActivated);
                    return new ResponseEntity(response.object, HttpStatus.OK);
                }

                // Cek akun aktif
                if (!globalFunction.checkAccountActivatedByAccountId(request.getAccount_id(), request.getCompany_id())) {
                    response.object.setMessage(English.userNotFound);
                    return new ResponseEntity(response.object, HttpStatus.OK);
                }

                updateQuery.updateAccountActivated(conn, request.getAccount_id());

                AccountEntity account = getFunction.getAccountByAccountId(request.getAccount_id(), request.getCompany_id());

                if (globalFunction.checkActivityConfig(request.getAccount_id(), request.getCompany_id())) {
                    serviceFunction.generateActivity(
                            serviceFunction.generateID("dat_activity"),
                            account.getAccountId(),
                            ("Activated for account " + account.getUsername()),
                            "activation",
                            null,
                            "auth",
                            "api/activated_account"
                    );
                }

                serviceFunction.generateLastAction(account.getAccountId(), account.getCompanyId());

                conn.commit();
                response.object.setStatus(English.success);
                response.object.setMessage(English.activatedAccountSuccess);
                return new ResponseEntity(response.object, HttpStatus.OK);

            } catch (SQLException sqlexcep) {
                conn.rollback();
                sqlexcep.printStackTrace();
                response.object.setMessage(English.internalServerError);
                return new ResponseEntity(response.object, HttpStatus.INTERNAL_SERVER_ERROR);
            } catch (ParseException peexcep) {
                conn.rollback();
                peexcep.printStackTrace();
                response.object.setMessage(English.internalServerError);
                return new ResponseEntity(response.object, HttpStatus.INTERNAL_SERVER_ERROR);
            } finally {
                conn.close();
            }

        } catch (SQLException e) {
            e.printStackTrace();
            response.object.setMessage(English.internalServerError);
            return new ResponseEntity(response.object, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
