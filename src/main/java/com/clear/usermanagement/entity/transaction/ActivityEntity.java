package com.clear.usermanagement.entity.transaction;

import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Data
@Table(name = "dat_activity")
public class ActivityEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "activity_id", length = 11, nullable = false)
    private Integer activityId;
    
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name="uuid", strategy ="org.hibernate.id.UUIDGenerator")
    private String uuid;

    @NotNull
    @Column(name = "account_id", length = 11, nullable = false)
    private Integer accountId;

    @NotBlank
    @Column(name = "modules", length = 100, nullable = false)
    private String modules;

    @Column(name = "module_id", length = 11, nullable = true)
    private Integer moduleId;

    @NotBlank
    @Column(name = "activity", length = 255, nullable = false)
    @Type(type = "text")
    private String activity;

    @NotBlank
    @Column(name = "activity_type", length = 50, nullable = false)
    private String activityType;

    @NotBlank
    @Column(name = "url", length = 255, nullable = false)
    private String url;

    @NotNull
    @Column(name = "created_by", length = 25, nullable = false)
    private Integer createdBy;

    @CreationTimestamp
    @Column(name = "created_at", nullable = false, insertable = false, updatable = false, columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Column(name = "updated_by", length = 25, nullable = true)
    private Integer updatedBy;

    @UpdateTimestamp
    @Column(name = "updated_at", insertable = false, updatable = true, nullable = true)
    @Temporal(TemporalType.DATE)
    private Date updatedAt;

}
