package com.clear.usermanagement.entity.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Data
@Table(name = "ref_api_management")
public class ApiManagementEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "api_management_id", length = 100, nullable = false)
    private Integer apiManagementId;

    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String uuid;

    @NotBlank
    @Column(name = "url", length = 250, nullable = false)
    private String url;

    @NotBlank
    @Column(name = "menu_class", length = 50, nullable = false)
    private String menuClass;

    @NotBlank
    @Column(name = "button_class", length = 50, nullable = false)
    private String buttonClass;

}
