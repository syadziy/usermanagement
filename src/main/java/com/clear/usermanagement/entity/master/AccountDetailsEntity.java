package com.clear.usermanagement.entity.master;

import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Data
@Table(name = "ref_account_details")
public class AccountDetailsEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "account_details_id", length = 11, nullable = false)
    private Integer accountDetailsId;
    
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name="uuid", strategy ="org.hibernate.id.UUIDGenerator")
    private String uuid;

    @Column(name = "account_id", length = 11, nullable = true)
    private Integer accountId;

    @Column(name = "fullname", length = 100, nullable = true)
    private String fullname;

    @Column(name = "employment_number", length = 25, nullable = true)
    private String employmentNumber;

    @Column(name = "city", length = 100, nullable = true)
    private String city;

    @Column(name = "country", length = 100, nullable = true)
    private String country;

    @Column(name = "locale", length = 100, nullable = true)
    private String locale;

    @Column(name = "address", length = 255, nullable = true)
    @Type(type = "text")
    private String address;

    @Column(name = "phone", length = 25, nullable = true)
    private String phone;

    @Column(name = "mobile", length = 25, nullable = true)
    private String mobile;

    @Column(name = "photo", length = 255, nullable = true)
    @Type(type = "text")
    private String photo;

    @Column(name = "joining_date", length = 25, nullable = true)
    private String joiningDate;

    @Column(name = "date_of_birth", length = 25, nullable = true)
    private String dateOfBirth;

    @Column(name = "present_address", length = 255, nullable = true)
    @Type(type = "text")
    private String presentAddress;

    @Column(name = "gender", length = 25, nullable = true)
    private String gender;

    @Column(name = "maratial_status", length = 25, nullable = true)
    private String maratialStatus;

    @Column(name = "father_name", length = 100, nullable = true)
    private String fatherName;

    @Column(name = "mother_name", length = 100, nullable = true)
    private String motherName;

    @NotNull
    @Column(name = "company_id", length = 11, nullable = false)
    private Integer companyId;

    @NotNull
    @Column(name = "status", length = 1, nullable = false, columnDefinition = "integer default 1")
    private Integer status;

    @NotNull
    @Column(name = "created_by", length = 25, nullable = false)
    private Integer createdBy;

    @CreationTimestamp
    @Column(name = "created_at", nullable = false, insertable = false, updatable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Column(name = "updated_by", length = 25, nullable = true)
    private Integer updatedBy;

    @UpdateTimestamp
    @Column(name = "updated_at", insertable = false, updatable = true, nullable = true)
    @Temporal(TemporalType.DATE)
    private Date updatedAt;

}
