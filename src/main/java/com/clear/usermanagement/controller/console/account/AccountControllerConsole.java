package com.clear.usermanagement.controller.console.account;

import com.clear.usermanagement.language.English;
import com.clear.usermanagement.request.account.AccountRequest;
import com.clear.usermanagement.response.Response;
import com.clear.usermanagement.service.AccountService;
import com.fasterxml.jackson.core.JsonProcessingException;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/account")
public class AccountControllerConsole {

    private final AccountService accountService;

    @Autowired
    public AccountControllerConsole(
            AccountService accountService
    ) {
        this.accountService = accountService;
    }

    @GetMapping("/get_all_account")
    public ResponseEntity getAllAccount() {
        Response response = new Response();
        response.object.setStatus(English.failed);

        try {
            String url = "get_all_account";
            return accountService.getAllData(response, url);
        } catch (JsonProcessingException e) {
            response.object.setMessage(English.internalServerError);
            return new ResponseEntity(response.object, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/get_detail_account/{uuid}")
    public ResponseEntity getDetailAccount(@PathVariable String uuid) {
        Response response = new Response();
        response.object.setStatus(English.failed);

        try {
            String url = "get_detail_account";
            return accountService.getDetailData(response, url, uuid);
        } catch (JsonProcessingException e) {
            response.object.setMessage(English.internalServerError);
            return new ResponseEntity(response.object, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/create_account")
    public ResponseEntity createAccount(@Valid @RequestBody AccountRequest request, Errors errors, BindingResult result) {
        Response response = new Response();
        response.object.setStatus(English.failed);

        if (errors.hasErrors()) {
            FieldError errorUsername = result.getFieldError("username");
            FieldError errorPassword = result.getFieldError("password");
            FieldError errorEmail = result.getFieldError("email");
            FieldError errorCompanyId = result.getFieldError("company_id");

            if (errorUsername != null) {
                response.object.setMessage(English.usernameNull);
                return new ResponseEntity(response.object, HttpStatus.OK);
            } else if (errorPassword != null) {
                response.object.setMessage(English.passwordNull);
                return new ResponseEntity(response.object, HttpStatus.OK);
            } else if (errorEmail != null) {
                response.object.setMessage(English.emailNull);
                return new ResponseEntity(response.object, HttpStatus.OK);
            } else if (errorCompanyId != null) {
                response.object.setMessage(English.companyIdNull);
                return new ResponseEntity(response.object, HttpStatus.OK);
            }
        }

        try {
            String url = "create_account";
            return accountService.createData(response, request, url);
        } catch (JsonProcessingException e) {
            response.object.setMessage(English.internalServerError);
            return new ResponseEntity(response.object, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
