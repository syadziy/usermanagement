package com.clear.usermanagement.service;

import com.clear.usermanagement.request.auth.CreateSuperadminRequest;
import com.clear.usermanagement.response.Response;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.ResponseEntity;

public interface CreateSuperadminService {
    
    ResponseEntity createSuperadmin(Response response, CreateSuperadminRequest request) throws JsonProcessingException;
    
}
