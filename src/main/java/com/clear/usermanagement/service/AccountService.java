package com.clear.usermanagement.service;

import com.clear.usermanagement.request.account.AccountRequest;
import com.clear.usermanagement.response.Response;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.ResponseEntity;

public interface AccountService {
    
    ResponseEntity getAllData(Response response, String url) throws JsonProcessingException;
    ResponseEntity getDetailData(Response response, String url, String uuid) throws JsonProcessingException;
    ResponseEntity createData(Response response, AccountRequest request, String url) throws JsonProcessingException;
    
}
