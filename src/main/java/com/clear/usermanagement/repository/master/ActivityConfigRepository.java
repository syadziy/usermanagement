package com.clear.usermanagement.repository.master;

import com.clear.usermanagement.entity.master.ActivityConfigEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ActivityConfigRepository extends PagingAndSortingRepository<ActivityConfigEntity, Integer> {

    @Query(value = "select ac from ActivityConfigEntity ac where companyId = :companyId and status = '1'")
    public ActivityConfigEntity findByCompanyId(@Param("companyId") Integer companyId);
    
}
