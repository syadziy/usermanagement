package com.clear.usermanagement.repository.master;

import com.clear.usermanagement.entity.master.AccountEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends PagingAndSortingRepository<AccountEntity, Integer> {

    @Query(value = "select a from AccountEntity a where token = :token")
    public AccountEntity findByToken(@Param("token") String token);
    
    @Query(value = "select a from AccountEntity a where tokenMobile = :tokenMobile")
    public AccountEntity findByTokenMobile(@Param("tokenMobile") String tokenMobile);

    @Query(value = "select a from AccountEntity a where (lower(username) = :username or lower(email) = :username) and companyId = null")
    public AccountEntity findByUsernameAdmin(@Param("username") String username);
    
    @Query(value = "select a from AccountEntity a where (lower(username) = :username or lower(email) = :username) and password = :password and companyId = null")
    public AccountEntity checkUserAdmin(@Param("username") String username, @Param("password") String password);
    
    @Query(value = "select a from AccountEntity a where (lower(username) = :username or lower(email) = :username) and companyId = null")
    public AccountEntity dataLoginAdmin(@Param("username") String username);
    
    @Query(value = "select a from AccountEntity a where (lower(username) = :username or lower(email) = :username) and companyId = :companyId")
    public AccountEntity findByUsername(@Param("username") String username, @Param("companyId") Integer companyId);
    
    @Query(value = "select a from AccountEntity a where (lower(username) = :username or lower(email) = :username) and password = :password and companyId = :companyId")
    public AccountEntity checkUser(@Param("username") String username, @Param("password") String password, @Param("companyId") Integer companyId);
    
    @Query(value = "select a from AccountEntity a where (lower(username) = :username or lower(email) = :username) and companyId = :companyId")
    public AccountEntity dataLogin(@Param("username") String username, @Param("companyId") Integer companyId);
    
    @Query(value = "select a from AccountEntity a where token = :token")
    public AccountEntity dataToken(@Param("token") String token);
    
    @Query(value = "select a from AccountEntity a where tokenMobile = :tokenMobile")
    public AccountEntity dataTokenMobile(@Param("tokenMobile") String tokenMobile);
    
    @Query(value = "select a from AccountEntity a where accountId = :accountId and companyId = :companyId and activated = '1' and banned = '0'")
    public AccountEntity findByUserActivatedByAccountId(@Param("accountId") Integer accountId, @Param("companyId") Integer companyId);
    
    @Query(value = "select a from AccountEntity a where accountId = :accountId and companyId = :companyId and activated = '0' and banned = '0'")
    public AccountEntity findByUserNotActivatedByAccountId(@Param("accountId") Integer accountId, @Param("companyId") Integer companyId);
    
    @Query(value = "select a from AccountEntity a where accountId = :accountId and companyId = :companyId and banned = '1'")
    public AccountEntity findByUserBannedByAccountId(@Param("accountId") Integer accountId, @Param("companyId") Integer companyId);

}
