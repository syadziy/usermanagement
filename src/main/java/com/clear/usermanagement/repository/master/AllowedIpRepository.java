package com.clear.usermanagement.repository.master;

import com.clear.usermanagement.entity.master.AllowedIpEntity;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AllowedIpRepository extends PagingAndSortingRepository<AllowedIpEntity, Integer>{
    
    @Query(value = "select ai from AllowedIpEntity ai where companyId = :companyId and status = '1'")
    public List<AllowedIpEntity> findByCompanyId(@Param("companyId") Integer companyId);
    
}
