package com.clear.usermanagement.repository.master;

import com.clear.usermanagement.entity.master.TokenConfigEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TokenConfigRepository extends PagingAndSortingRepository<TokenConfigEntity, Integer> {

    @Query(value = "select tc from TokenConfigEntity tc where companyId = :companyId")
    public TokenConfigEntity findByCompanyId(@Param("companyId") Integer companyId);

}
