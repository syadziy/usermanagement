package com.clear.usermanagement.repository.master;

import com.clear.usermanagement.entity.master.AllowedIpConfigEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AllowedIpConfigRepository extends PagingAndSortingRepository<AllowedIpConfigEntity, Integer> {

    @Query(value = "select aip from AllowedIpConfigEntity aip where companyId = :companyId and status = '1'")
    public AllowedIpConfigEntity findByCompanyId(@Param("companyId") Integer companyId);

}
